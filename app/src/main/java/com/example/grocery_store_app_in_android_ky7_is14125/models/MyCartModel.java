package com.example.grocery_store_app_in_android_ky7_is14125.models;


import java.io.Serializable;

public class MyCartModel implements Serializable {
        String productName;
        String productPrice;
        String productDate;
        String productTime;
        String totalQuantity;
        int totalPrice;
        String documentId;

        public MyCartModel() {
        }

        public MyCartModel(String productName, String productPrice, String currentDate, String currentTime, String totalQuantity, int totalPrice) {
            this.productName = productName;
            this.productPrice = productPrice;
            this.productDate = currentDate;
            this.productTime = currentTime;
            this.totalQuantity = totalQuantity;
            this.totalPrice = totalPrice;
        }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public String getProductPrice() {
            return productPrice;
        }

        public void setProductPrice(String productPrice) {
            this.productPrice = productPrice;
        }

        public String getProductDate() {
            return productDate;
        }

        public void setProductDate(String productDate) {
            this.productDate = productDate;
        }

        public String getProductTime() {
            return productTime;
        }

        public void setProductTime(String productTime) {
            this.productTime = productTime;
        }

        public String getTotalQuantity() {
            return totalQuantity;
        }

        public void setTotalQuantity(String totalQuantity) {
            this.totalQuantity = totalQuantity;
        }

        public int getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(int totalPrice) {
            this.totalPrice = totalPrice;
        }
    }


