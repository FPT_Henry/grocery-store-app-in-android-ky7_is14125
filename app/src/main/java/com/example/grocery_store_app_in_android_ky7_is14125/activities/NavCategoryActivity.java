package com.example.grocery_store_app_in_android_ky7_is14125.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.grocery_store_app_in_android_ky7_is14125.R;
import com.example.grocery_store_app_in_android_ky7_is14125.adapter.NavCategoryDetailedAdapter;
import com.example.grocery_store_app_in_android_ky7_is14125.models.HomeCategory;
import com.example.grocery_store_app_in_android_ky7_is14125.models.NavCategoryDetailedModel;
import com.example.grocery_store_app_in_android_ky7_is14125.models.ViewAllModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class NavCategoryActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    List<NavCategoryDetailedModel> list;
    NavCategoryDetailedAdapter adapter;
    FirebaseFirestore firestore;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_category);
        firestore = FirebaseFirestore.getInstance();
        progressBar = findViewById(R.id.progressbar);


        recyclerView = findViewById(R.id.nav_cat_det_rec);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        list = new ArrayList<>();
        adapter = new NavCategoryDetailedAdapter(this, list);
        recyclerView.setAdapter(adapter);

        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

        //lay data type from fragmentCategory
        String type = getIntent().getStringExtra("type");

        //getting data firebase follow type
        if(type != null && type.equalsIgnoreCase("drink")){
            firestore.collection("NavCategoryDetailed")
                    .whereEqualTo("type", "drink")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    NavCategoryDetailedModel model = document.toObject(NavCategoryDetailedModel.class);
                                    list.add(model);
                                    adapter.notifyDataSetChanged();
                                    progressBar.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                }
                            } else {
                                Log.w("", "Error getting documents.", task.getException());
                            }
                        }
                    });
        }

        if(type != null && type.equalsIgnoreCase("breakfast")){
            firestore.collection("NavCategoryDetailed")
                    .whereEqualTo("type", "breakfast")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    NavCategoryDetailedModel model = document.toObject(NavCategoryDetailedModel.class);
                                    list.add(model);
                                    adapter.notifyDataSetChanged();
                                    progressBar.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                }
                            } else {
                               Toast.makeText(getApplicationContext(), "Error getting documents."+ task.getException(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
        }
}}