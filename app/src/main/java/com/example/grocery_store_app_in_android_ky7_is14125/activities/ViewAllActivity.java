package com.example.grocery_store_app_in_android_ky7_is14125.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.example.grocery_store_app_in_android_ky7_is14125.R;
import com.example.grocery_store_app_in_android_ky7_is14125.adapter.ViewAllAdapter;
import com.example.grocery_store_app_in_android_ky7_is14125.models.ViewAllModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

public class ViewAllActivity extends AppCompatActivity {

    List<ViewAllModel> list;
    ViewAllAdapter viewAllAdapter;
    RecyclerView recyclerView;


    FirebaseFirestore firestore;
    Toolbar toolbar;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all);
        firestore = FirebaseFirestore.getInstance();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.view_all_rec);
        progressBar = findViewById(R.id.progressbar);

        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);

        recyclerView.setLayoutManager( new LinearLayoutManager(this));
        list = new ArrayList<>();
        viewAllAdapter = new ViewAllAdapter(getApplicationContext(), list);
        recyclerView.setAdapter(viewAllAdapter);

        String type = getIntent().getStringExtra("type");



        //getting fruit
            if(type != null && type.equalsIgnoreCase("fruit")){
                firestore.collection("AllProducts")
                        .whereEqualTo("type", "fruit")
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (QueryDocumentSnapshot document : task.getResult()) {
                                        ViewAllModel viewAllModel = document.toObject(ViewAllModel.class);
                                        list.add(viewAllModel);
                                        viewAllAdapter.notifyDataSetChanged();
                                        progressBar.setVisibility(View.GONE);
                                        recyclerView.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    Log.w("", "Error getting documents.", task.getException());
                                }
                            }
                        });
            }


        //getting vegetable
        if(type != null && type.equalsIgnoreCase("vegetable")){
            firestore.collection("AllProducts")
                    .whereEqualTo("type", "vegetable")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    ViewAllModel viewAllModel = document.toObject(ViewAllModel.class);
                                    list.add(viewAllModel);
                                    viewAllAdapter.notifyDataSetChanged();
                                    progressBar.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                }
                            } else {
                                Log.w("", "Error getting documents.", task.getException());
                            }
                        }
                    });
        }

        //getting vegetable
        if(type != null && type.equalsIgnoreCase("egg")){
            firestore.collection("AllProducts")
                    .whereEqualTo("type", "egg")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    ViewAllModel viewAllModel = document.toObject(ViewAllModel.class);
                                    list.add(viewAllModel);
                                    viewAllAdapter.notifyDataSetChanged();
                                    progressBar.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                }
                            } else {
                                Log.w("", "Error getting documents.", task.getException());
                            }
                        }
                    });
        }

        //getting vegetable
        if(type != null && type.equalsIgnoreCase("milk")){
            firestore.collection("AllProducts")
                    .whereEqualTo("type", "milk")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    ViewAllModel viewAllModel = document.toObject(ViewAllModel.class);
                                    list.add(viewAllModel);
                                    viewAllAdapter.notifyDataSetChanged();
                                    progressBar.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                }
                            } else {
                                Log.w("", "Error getting documents.", task.getException());
                            }
                        }
                    });
        }

        //getting vegetable
        if(type != null && type.equalsIgnoreCase("fish")){
            firestore.collection("AllProducts")
                    .whereEqualTo("type", "fish")
                    .get()
                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()) {
                                for (QueryDocumentSnapshot document : task.getResult()) {
                                    ViewAllModel viewAllModel = document.toObject(ViewAllModel.class);
                                    list.add(viewAllModel);
                                    viewAllAdapter.notifyDataSetChanged();
                                    progressBar.setVisibility(View.GONE);
                                    recyclerView.setVisibility(View.VISIBLE);
                                }
                            } else {
                                Log.w("", "Error getting documents.", task.getException());
                            }
                        }
                    });
        }
    }
}