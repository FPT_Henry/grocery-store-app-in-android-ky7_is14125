package com.example.grocery_store_app_in_android_ky7_is14125.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.grocery_store_app_in_android_ky7_is14125.R;
import com.google.firebase.auth.FirebaseAuth;

public class HomeActivity extends AppCompatActivity {
    private ProgressBar progressBar;

    FirebaseAuth  auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        progressBar = findViewById(R.id.progressbar);
        progressBar.setVisibility(View.GONE);

        auth = FirebaseAuth.getInstance();
        if(auth.getCurrentUser() != null){
            progressBar.setVisibility(View.VISIBLE);
            Toast.makeText(this, "please wait you are already logged in", Toast.LENGTH_SHORT).show();
        }

    }
    public void login(View view) {
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
    }

    public void registration(View view) {
        startActivity(new Intent(getApplicationContext(), RegistrationActivity.class));
    }
}