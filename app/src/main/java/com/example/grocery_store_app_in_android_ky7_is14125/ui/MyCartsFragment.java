package com.example.grocery_store_app_in_android_ky7_is14125.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.grocery_store_app_in_android_ky7_is14125.R;
import com.example.grocery_store_app_in_android_ky7_is14125.activities.PlacedOrderActivity;
import com.example.grocery_store_app_in_android_ky7_is14125.adapter.MyCartAdapter;
import com.example.grocery_store_app_in_android_ky7_is14125.models.MyCartModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MyCartsFragment extends Fragment {

    RecyclerView recyclerView;
    MyCartAdapter cartAdapter;
    List<MyCartModel> cartModelList;
    TextView overTotalAmount;

    FirebaseFirestore db;
    FirebaseAuth auth;
    ProgressBar progressBar;

    Button buyNow;

    public MyCartsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my_carts, container, false);
        db = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();


        progressBar = root.findViewById(R.id.progressbar);
        recyclerView = root.findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        progressBar.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        buyNow = root.findViewById(R.id.buy_now);

        overTotalAmount = root.findViewById(R.id.textTotal);
        LocalBroadcastManager.getInstance(getActivity())
                .registerReceiver(broadcastReceiver,new IntentFilter("MyToTalAmount"));


        cartModelList = new ArrayList<>();
        cartAdapter = new MyCartAdapter(getActivity(), cartModelList);
        recyclerView.setAdapter(cartAdapter);

        db.collection("CurrentUser").document(auth.getCurrentUser().getUid())
                .collection("AddToCart").get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (DocumentSnapshot documentSnapshot : task.getResult().getDocuments()) {
                        String documentId = documentSnapshot.getId();

                        MyCartModel cartModel = documentSnapshot.toObject(MyCartModel.class);

                        cartModel.setDocumentId(documentId);

                        if (check(cartModel)) {
                            for (MyCartModel cart : cartModelList) {
                                if (cart.getProductName().equals(cartModel.getProductName())) {
                                    int totalQuantity = Integer.parseInt(cartModel.getTotalQuantity())
                                            + Integer.parseInt(cart.getTotalQuantity());

                                    cart.setTotalQuantity(totalQuantity + "");
                                    cart.setTotalPrice(cart.getTotalPrice() + cartModel.getTotalPrice());
                                    progressBar.setVisibility(View.VISIBLE);
                                    recyclerView.setVisibility(View.GONE);
                                }
                            }
                        } else {
                            cartModelList.add(cartModel);
                        }
                        progressBar.setVisibility(View.GONE);
                        recyclerView.setVisibility(View.VISIBLE);
                    }
                    calculateTotalAmount(cartModelList);
                }
            }
        });

        buyNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), PlacedOrderActivity.class);
                intent.putExtra("itemList", (Serializable) cartModelList);
                startActivity(intent);
            }
        });


        return root;
    }
    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int totalBill = intent.getIntExtra("totalAmount", 0);
            overTotalAmount.setText("ToTal BIll:"+ totalBill +"$");
        }
    };

    public boolean check(MyCartModel cartModel) {
        for (MyCartModel cart : cartModelList) {
            if (cart.getProductName().equals(cartModel.getProductName())) {
                return true;
            }
        }
        return false;
    }

    private void calculateTotalAmount(List<MyCartModel> cartModelList) {
        double totalAmount = 0.0;
        for (MyCartModel myCartModel : cartModelList) {
            totalAmount += myCartModel.getTotalPrice();

        }
    }
}